<template>
  <div>
    <hero-section
      :title="`${category.icon ?? ''} ${category.name}`"
    >
      <template
        v-if="category.summary"
        #subtitle
      >
        <!-- eslint-disable-next-line vue/no-v-html -->
        <div v-html="category.summary" />
      </template>
    </hero-section>
    <section class="container">
      <main class="content">
        <div class="columns is-multiline">
          <div
            v-for="plant in category.plants"
            :key="plant.id"
            class="column is-one-third-tablet"
          >
            <nuxt-link :to="localePath({ name: 'plant-category-plant', params: { category: category.id, plant: plant.id } })">
              <article class="message">
                <div class="message-header">
                  <span>{{ plant.name }}</span>
                  <span>⧉</span>
                </div>
                <div class="message-body">
                  <div class="field is-grouped is-grouped-multiline">
                    <div
                      v-for="[kind, level] in plant.needs"
                      :key="kind"
                      class="control"
                    >
                      <div class="tags has-addons">
                        <span class="tag is-dark">
                          <template v-if="kind === 'watering'">💧</template>
                          <template v-else-if="kind === 'light'">☀️</template>
                          <template v-else>{{ kind }}</template>
                        </span>
                        <span
                          class="tag"
                          :class="{ 'is-info': kind === 'watering', 'is-warning': kind === 'light' }"
                        >
                          <template v-if="level === 'low'">{{ $t('low') }}</template>
                          <template v-else-if="level === 'medium'">{{ $t('medium') }}</template>
                          <template v-else-if="level === 'high'">{{ $t('high') }}</template>
                          <template v-else>{{ level }}</template>
                        </span>
                      </div>
                    </div>
                  </div>
                  <dl>
                    <dt v-if="plant.average_weight">
                      {{ $t('average_weight') }}
                    </dt>
                    <dd v-if="plant.average_weight">
                      {{ plant.average_weight }}
                    </dd>
                    <dt v-if="plant.distance_between_plants">
                      {{ $t('distance_between_plants') }}
                    </dt>
                    <dd v-if="plant.distance_between_plants">
                      {{ plant.distance_between_plants }}
                    </dd>
                  </dl>
                </div>
                <div
                  v-if="plant.summary"
                  class="message-body"
                >
                  <!-- eslint-disable-next-line vue/no-v-html -->
                  <div v-html="plant.summary" />
                </div>
              </article>
            </nuxt-link>
          </div>
        </div>
      </main>
    </section>
  </div>
</template>

<script setup>
import { useLocalePath } from '#i18n'

const { params } = useRoute()
const { getPlantCategory } = usePlant()
const localePath = useLocalePath()

const category = await getPlantCategory(params.category)
</script>

<style lang="scss" scoped>
main {
  margin: 20px 0;
}
</style>

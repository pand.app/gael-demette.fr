import { useDirectusItems, useDirectusFiles } from '#imports'

export const useTea = () => {
  const { getItemById, getItems } = useDirectusItems()
  const { getThumbnail } = useDirectusFiles()

  const targetLang = 'fr-FR'
  const fallbackLang = 'en-US'
  const lang = [targetLang, fallbackLang]

  const resolveTranslation = function resolveTranslation(...keys) {
    return keys.reduce(
      (acc, key) => ({
        ...acc,
        [key]: this.translations.find(t => t.languages_code === targetLang)?.[key]
          ?? this.translations.find(t => t.languages_code === fallbackLang)?.[key],
      }),
      {},
    )
  }

  const langFilter = { _filter: { languages_code: { _in: lang } } }
  const deep = {
    translations: langFilter,
    who_like: { bubble_id: { translations: langFilter } },
    ingredients: { tea_ingredient_id: { translations: langFilter } },
    shop: { translations: langFilter },
  }
  const fields = [
    'celsius', 'max_time_to_infuse', 'min_time_to_infuse', 'picture', 'slug',
    'translations.name', 'translations.summary', 'translations.comments', 'translations.languages_code',
    'translations.link', 'shop.translations.name', 'shop.translations.languages_code',
    'ingredients.tea_ingredient_id.id', 'ingredients.tea_ingredient_id.has_theine',
    'ingredients.tea_ingredient_id.translations.languages_code', 'ingredients.tea_ingredient_id.translations.name',
    'who_like.bubble_id.id', 'who_like.bubble_id.translations.name', 'who_like.bubble_id.translations.languages_code',
  ]

  const fetchTea = async (slug: string) => {
    const rawTea = await getItemById({
      collection: 'tea',
      id: slug,
      params: { deep, fields },
    })

    return {
      id: rawTea.slug,
      slug: rawTea.slug,
      celsius: rawTea.celsius,
      picture: rawTea.picture,
      max_time_to_infuse: rawTea.max_time_to_infuse,
      min_time_to_infuse: rawTea.min_time_to_infuse,
      ...resolveTranslation.call(rawTea, 'name', 'summary', 'comments', 'link'),
      shop: resolveTranslation.call(rawTea.shop, 'name'),
      who_like: rawTea.who_like.map(wl => ({
        id: wl.bubble_id.id,
        ...resolveTranslation.call(wl.bubble_id, 'name'),
      })),
      ingredients: rawTea.ingredients.map(wl => ({
        id: wl.tea_ingredient_id.id,
        has_theine: wl.tea_ingredient_id.has_theine,
        ...resolveTranslation.call(wl.tea_ingredient_id, 'name'),
      })),
    }
  }

  const fetchTeaPurchaseStats = async (slug: string) => {
    return (await getItems({
      collection: 'tea_purchase',
      params: {
        filter: { tea: { _eq: slug } },
        aggregate: JSON.stringify({
          sum: 'quantity_in_grams',
          avg: 'price_per_kilogram',
          min: 'price_per_kilogram',
          max: 'price_per_kilogram',
          count: '*',
        }),
      },
    })).shift()
  }

  const tagClassesFromBubbleId = (bubbleId: string) => {
    const classes = ['tag', 'is-light']
    switch (bubbleId) {
      case 'me':
        return [...classes, 'is-primary']
      case 'family':
        return [...classes, 'is-link']
      case 'children':
        return [...classes, 'is-info']
      default:
        return classes
    }
  }

  const fetchTeaList = async (query = {}) => {
    const filter = {}
    if (query?.ingredient) {
      filter.ingredients = { tea_ingredient_id: { id: { _eq: query?.ingredient } } }
    }

    const list = await getItems({
      collection: 'tea',
      params: { deep, fields, filter },
    })

    return list.map(row => ({
      slug: row.slug,
      celsius: row.celsius,
      max_time_to_infuse: row.max_time_to_infuse,
      min_time_to_infuse: row.min_time_to_infuse,
      ...resolveTranslation.call(row, 'name', 'summary'),
      shop: resolveTranslation.call(row.shop, 'name'),
      who_like: row.who_like.map(wl => ({ id: wl.bubble_id.id, ...resolveTranslation.call(wl.bubble_id, 'name') })),
    }))
  }

  return {
    fetchTea,
    fetchTeaList,
    fetchTeaPurchaseStats,
    tagClassesFromBubbleId,
    getThumbnail,
  }
}

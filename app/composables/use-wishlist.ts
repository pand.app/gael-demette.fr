import MarkdownIt from 'markdown-it'
import { useDirectusItems } from '#imports'

type LanguageCode = 'fr-FR' | 'en-US'

interface Translation {
  languages_code: LanguageCode
  name?: string
  summary?: string
  description?: string
  criteria?: string
}

interface Translatable {
  translations: Translation[]
}

interface Gift extends Translatable {
  id: string
  order: number
  needed: boolean
  name: string
  description: string | null
  criteria: string | null
}

interface GiftCategory extends Translatable {
  id: string
  name: string
  summary: string | null
  gifts: Gift[]
}

type TranslatedFields<T extends Translatable> = Omit<T, 'translations'> & {
  [K in keyof Translation]: K extends 'languages_code' ? never : Translation[K]
}

type TranslatedGift = TranslatedFields<Gift>
type TranslatedGiftCategory = TranslatedFields<GiftCategory> & { gifts: TranslatedGift[] }

export const useWishlist = () => {
  const { getItems } = useDirectusItems()
  const md = new MarkdownIt()

  const targetLang: LanguageCode = 'fr-FR'
  const fallbackLang: LanguageCode = 'en-US'
  const lang = [targetLang, fallbackLang] as const

  const resolveTranslation = function resolveTranslation<T extends Translatable>(this: T, ...keys: Array<keyof Translation>): Partial<TranslatedFields<T>> {
    return keys.reduce(
      (acc, key) => ({
        ...acc,
        [key]: this.translations.find(t => t.languages_code === targetLang)?.[key]
          ?? this.translations.find(t => t.languages_code === fallbackLang)?.[key],
      }),
      {},
    )
  }

  const fetchWishlist = async (): Promise<TranslatedGiftCategory[]> => {
    const langFilter = { _filter: { languages_code: { _in: lang } } }
    const deep = { translations: langFilter }
    const filter = {}
    const fields = [
      'id',
      'translations.languages_code',
      'translations.name',
      'translations.summary',
      'gifts.id',
      'gifts.order',
      'gifts.needed',
      'gifts.translations.languages_code',
      'gifts.translations.description',
      'gifts.translations.criteria',
      'gifts.translations.name',
    ] as const

    const categories = await getItems<GiftCategory>({
      collection: 'gift_category',
      params: { deep, fields, filter },
    })

    return categories
      .map(row => ({
        ...row,
        ...resolveTranslation.call(row, 'name', 'summary'),
        gifts: row.gifts.map(g => ({
          ...g,
          ...resolveTranslation.call(g, 'name', 'criteria', 'description'),
        })),
      }))
      .map(row => ({
        ...row,
        summary: row.summary ? md.render(row.summary) : null,
        gifts: row.gifts.map(g => ({
          ...g,
          criteria: g.criteria ? md.render(g.criteria) : null,
          description: g.description ? md.render(g.description) : null,
        })),
      }))
      .filter(r => r.gifts.length > 0)
  }

  return {
    fetchWishlist,
  }
}

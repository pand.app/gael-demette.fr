import { Temporal } from '@js-temporal/polyfill'
import { useI18n } from 'vue-i18n'
import { merge } from 'lodash-es'
import frCvData from '../data/cv/fr.json'
import enCvData from '../data/cv/en.json'
import baseCvData from '../data/cv/base.json'

export function useCV() {
  const { locale } = useI18n()

  // Get CV data based on current locale
  const getCvData = () => {
    const localizedData = locale.value === 'en' ? enCvData : frCvData
    // Deep merge base data with localized data
    return merge({}, baseCvData, localizedData)
  }

  // Calculate age based on birthday using Temporal API
  const calculateAge = () => {
    const birthDate = Temporal.PlainDate.from(getCvData().personal.birthday)
    const today = Temporal.Now.plainDateISO()

    return today.since(birthDate, { largestUnit: 'years' }).years
  }

  // Format date for display using Temporal API
  const formatDate = (dateString: string) => {
    if (!dateString) return ''

    // Handle YYYY-MM format (add day if missing)
    const normalizedDate = dateString.length === 7 ? `${dateString}-01` : dateString

    const date = Temporal.PlainDate.from(normalizedDate)
    return date.toLocaleString(locale.value, { month: 'short', year: 'numeric' })
  }

  // Sort technologies by different criteria
  const sortTechnologies = (category: 'development' | 'sysadmin' | 'tools' | 'soft', sortBy: 'preference' | 'lastUsed' | 'name' = 'lastUsed') => {
    const data = getCvData().technologies[category]

    // Convert object to array format for consistent sorting
    const technologies = Object.entries(data).map(([id, tech]) => ({
      id,
      ...tech,
    }))

    // For soft skills, only sort by name
    if (category === 'soft') {
      return technologies.sort((a, b) => a.name.localeCompare(b.name, locale.value))
    }

    // For other categories, sort by the specified criteria
    if (sortBy === 'preference') {
      return technologies.sort((a, b) => b.preference - a.preference)
    }
    else if (sortBy === 'lastUsed') {
      return technologies.sort((a, b) => {
        // If lastUsed is null, it means the technology is currently in use
        // So it should appear at the top of the list
        if (a.lastUsed === null) return -1
        if (b.lastUsed === null) return 1

        const dateA = new Date(a.lastUsed)
        const dateB = new Date(b.lastUsed)
        return dateB.getTime() - dateA.getTime()
      })
    }
    else {
      return technologies.sort((a, b) => a.name.localeCompare(b.name, locale.value))
    }
  }

  return {
    get personal() { return getCvData().personal },
    get experiences() { return getCvData().experiences },
    get education() { return getCvData().education },
    get technologies() { return getCvData().technologies },
    get lastUpdateDate() { return getCvData().lastUpdateDate },
    calculateAge,
    formatDate,
    sortTechnologies,
  }
}

// https://v3.nuxtjs.org/api/configuration/nuxt.config

export default defineNuxtConfig({
  modules: [
    'nuxt-directus',
    '@nuxt/eslint',
    'nuxt-security',
    'nuxt-purgecss',
    '@nuxtjs/robots',
    'nuxt-schema-org',
    'nuxt-og-image',
    '@nuxtjs/i18n',
  ],
  app: {
    title: 'Gaël Demette',
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
    },
  },
  css: [
    { src: '@/assets/css/main.scss', lang: 'scss' },
  ],
  site: { url: 'https://gael-demette.fr' }, future: {
    compatibilityVersion: 4,
  },
  compatibilityDate: '2024-11-14',
  buildModules: [
    '@nuxtjs/pwa',
  ],
  directus: {
    autoFetch: false,
    url: 'https://directus.pand.app/',
  },
  eslint: {
    config: {
      stylistic: true,
    },
  },
  i18n: {
    locales: [
      {
        code: 'fr',
        iso: 'fr-FR',
        name: 'Français',
        file: 'fr.json',
      },
      {
        code: 'en',
        iso: 'en-UK',
        name: 'English',
        file: 'en.json',
      },
    ],
    defaultLocale: 'fr',
    strategy: 'prefix_except_default',
    langDir: '../locales',
    detectBrowserLanguage: false,
    compilation: {
      strictMessage: false,
    },
  },

  loading: { color: '#588C7E' },
  manifest: {
    theme_color: '#588C7E',
  },
  security: {
    headers: {
      contentSecurityPolicy: false,
    },
  },
})

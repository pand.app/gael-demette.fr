# Build stage
FROM node:22 AS builder

WORKDIR /app
COPY package.json pnpm-lock.yaml ./

RUN corepack enable && corepack prepare --activate
RUN pnpm config set store-dir /app/.pnpm-store
RUN pnpm install --frozen-lockfile

COPY . .
RUN pnpm run build

# Production stage using distroless
FROM gcr.io/distroless/nodejs22-debian12:nonroot

WORKDIR /app
COPY --from=builder /app/.output .

EXPOSE 3000
CMD ["/app/server/index.mjs"]